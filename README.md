## Laravel and React boilerplate with authentication scaffold

## Install Passport

Open a terminal window and install the passport using following command

 ```
 php artisan passport:install
 ```
## Update the Passport keys in .env file 
Copy the keys for personal and password grants in `.env` file

```
PERSONAL_CLIENT_ID=1
PERSONAL_CLIENT_SECRET=xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
PASSWORD_CLIENT_ID=2
PASSWORD_CLIENT_SECRET=xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

## Run PHP Dev Server
Either create a local dev url and map the link in webpack.mix.js file or open an other terminal window and copy paste the following command

```
php artisan serve
```

## Run Node Engine

Compile assets one time.
```
npm run dev
```

### Done

- Redux 
- Passport authentication
- Form validation; client and server
- User Model C.R.U.D.
- Tests
- React 16
- Laravel 5.6

### TODO
Add Laravel Repository, Services, Facades
Add UserProfile Model
...