<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Article
 *
 * @property-read \App\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article mine($user_id)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Article onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article published()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Article withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Article withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $userId
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $content
 * @property bool $published
 * @property \Carbon\Carbon|null $publishedAt
 * @property \Carbon\Carbon|null $deletedAt
 * @property \Carbon\Carbon|null $createdAt
 * @property \Carbon\Carbon|null $updatedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUserId($value)
 */
class Article extends Model
{
    // use soft delete instead of permanent delete
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'articles';

    protected $fillable = ['title', 'slug', 'description', 'content', 'published', 'published_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'published_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
            'published' => 'boolean',
    ];

    /**
     * Load all for admin and paginate
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function loadAll()
    {
        return static::latest()
                     ->paginate();
    }

    /**
     * Load all for logged in user and paginate
     *
     * @param $user_id
     *
     * @return mixed
     */
    public static function loadAllMine($user_id)
    {
        return static::latest()
                     ->mine($user_id)
                     ->paginate();
    }

    /**
     * load all published with pagination
     *
     * @return mixed
     */
    public static function loadAllPublished()
    {
        return static::with([
                'user' => function ($query) {
                    $query->select('id', 'name');
                },
        ])
                     ->latest()
                     ->published()
                     ->paginate();
    }

    /**
     * load one published
     *
     * @param $id
     *
     * @return mixed
     */
    public static function loadPublished($slug)
    {
        return static::with([
                'user' => function ($query) {
                    $query->select('id', 'name');
                },
        ])
                     ->published()
                     ->where('slug', $slug)
                     ->firstOrFail();
    }

    /**
     * Add query scope to get only published articles
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->where([
                'published' => true,
        ]);
    }

    /**
     * Load only articles related with the user id
     *
     * @param $query
     * @param $user_id
     *
     * @return mixed
     */
    public function scopeMine($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

    /**
     * Relationship between articles and user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
